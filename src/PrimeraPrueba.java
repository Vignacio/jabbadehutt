import java.util.Scanner;
public class PrimeraPrueba {

	public static void main(String[] args) {
		
		//designar
		byte numero1 = 89;
		int numero2 = 88888;
		short numero3 = (short) 46554;
		long numero4 = 96;
		float numero5 = 45;
		double numero6 = 89.56;
		char caracter = 'a';
		
		//datos no primitivos
		String nombre = "jose";
		Integer numero = 10;
		
		String otra = nombre.concat(" perez");
		double pi = Math.PI;
		
		
		
		System.out.println("Bienvenido a Java");
		
	}
	
}
